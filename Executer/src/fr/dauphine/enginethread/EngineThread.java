package fr.dauphine.enginethread;

import fr.dauphine.executor.Constants;
import fr.dauphine.executor.EngineThreadInfo;
import fr.dauphine.logger.ServiceData;
import fr.dauphine.message.Message;
import fr.dauphine.service.Service;
import fr.dauphine.serviceexecution.RunService;
import mpi.MPI;

public class EngineThread {
	
	private EngineThreadInfo et;
	private ServiceData serviceData;

	public void run() {
	
		initialize();
		
		waitInputs();
		
		execute();
		
		executeFinalPhase();
	}
	
	private void initialize() {
		
		boolean initialied = false;
		while(!initialied) {
			Message m = new Message();
			m.receive();
			if(m.isInitialize()) {
				et = m.getEngineThreadInfo();
				Message.setRanks(et.getRanks());
				System.out.println(et.getServiceName() +" rank: " + MPI.COMM_WORLD.Rank() + " == " + et.getRanks().get(et.getService()));
				initialied = true;
			}
		}
		serviceData = new ServiceData(et.getServiceName());
	}
	
	private void waitInputs() {
		while(!et.isFireable()) {
			Message m = new Message();
			m.receive();
			if(m.isDataMessage()) {
				System.out.println("service " + et.getServiceName() +
						" received input from " + m.getDataMessage().getSenderService());
				et.addInputsValues(m.getDataMessage().getData());
			}
		}
	}
	
	private void execute() {
		RunService rs = new RunService(et.getService());
		rs.run();
		
		serviceData.setExecutionTime(rs.getExecutionTime());
		serviceData.setSuccess(rs.getSuccess());
		serviceData.setTimeStamp(rs.getTimeStamp());
		
		et.setOutputs(rs.dummyGenerateOutputs());
		for(Service s:et.getSuccessors()) {
			System.out.println("service " + et.getServiceName() +
					" sending output to " + s);
			(new Message(et.getDataMessage(s),s)).send();
		}
	}
	
	private void executeFinalPhase() {
		Message m = new Message();
		m.receive();
		if(m.isFinalize())
			sendServiceData();
	}
	
	private void sendServiceData() {
		(new Message(serviceData, Constants.EXECUTION_ENGINE)).send();
	}
}
