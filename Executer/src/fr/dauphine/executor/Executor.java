package fr.dauphine.executor;

/**
 * An executor consisting of
 * This interface permits, but does not enforce, any of the following 
 * common variations of executors:
 * <ul>
 * <li> An executor using MPI
 * <li> An executor using Threads
 * </ul> 
 * Extensions or implementations of this interface 
 * may enforce or disallow any or all of these variations.
 * 
 * <p>Definitions (with respect to a given vertex <code>v</code>):
 * <ul>
 * <li/><b>incoming edge</b> of <code>v</code>: an edge that can be traversed 
 * from a neighbor of <code>v</code> to reach <code>v</code>
 * <li/><b>outgoing edge</b> of <code>v</code>: an edge that can be traversed
 * from <code>v</code> to reach some neighbor of <code>v</code> 
 * <li/><b>predecessor</b> of <code>v</code>: a vertex at the other end of an
 * incoming edge of <code>v</code>
 * <li/><b>successor</b> of <code>v</code>: a vertex at the other end of an 
 * outgoing edge of <code>v</code>
 * <li/>
 * </ul> 
 * 
 * @author Rafael Angarita
 */
public interface Executor {
	
	void execute();
	
}
