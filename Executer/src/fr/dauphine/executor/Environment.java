package fr.dauphine.executor;

import java.util.HashMap;
import java.util.Map;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.graphanalyser.GraphAnalyser;
import fr.dauphine.graphreader.GraphReader;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.query.Query;
import fr.dauphine.service.Constants;
import fr.dauphine.service.Service;

public class Environment {
	
	private Graph<Service, Number> graph;
	private String filename, qosFilename, dataFilename, queryFilename;
	private double estimatedExecutionTime;
	private Query q;
	private Map<Service, Integer> ranks;
	
	public Environment(String filename, String qosFilename, String dataFilename, String queryFilename) {
		this.filename = filename;
		this.qosFilename = qosFilename;
		this.dataFilename = dataFilename;
		this.queryFilename = queryFilename;
		this.ranks = new HashMap<Service, Integer>();
	}

	public void load() {
		graph = GraphReader.getGraph(filename, qosFilename, dataFilename);
		q = GraphReader.getQuery(queryFilename);
		GraphUtils.addControlNodes(graph, q);
		
		GraphAnalyser analyser = new GraphAnalyser(graph);
		
		estimatedExecutionTime = analyser.getEstimatedExecutionTime(Constants.INITIAL_NODE, Constants.FINAL_NODE);
		analyser.setVertexImportance();
		
		setRanks();
		System.out.println(ranks);
		
	}
	
	public Graph<Service, Number> getGraph() { 
		return graph;
	}
	
	public double getEstimatedExecutionTime() {
		return estimatedExecutionTime;
	}
	
	public Map<Service, Integer> getRanks() {
		return ranks;
	}
	
	private void setRanks() {
		int rank =1;
		for(Service s:graph.getVertices())
			if(!s.isControl())
				ranks.put(s, rank++);
			else
				ranks.put(s, 0);
	}

}
