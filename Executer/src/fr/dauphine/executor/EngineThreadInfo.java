package fr.dauphine.executor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import edu.uci.ics.jung.graph.Graph;
import fr.dauphine.message.DataMessage;
import fr.dauphine.service.Data;
import fr.dauphine.service.Service;

public class EngineThreadInfo implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private  Service service;
	private Map<Service, List<Data>> predecessors;
	private Map<Service, List<Data>> successors;
	private Map<Data, String> inputs;
	private Map<Data, String> outputs;
	private Map<Service, Integer> ranks;
	
	public EngineThreadInfo(Service service, Graph<Service, Number> graph,
			Map<Service, Integer> ranks) {
		
		this.service = service;
		this.ranks = ranks;
		predecessors = new HashMap<Service, List<Data>>();
		successors = new HashMap<Service, List<Data>>();
		inputs = new HashMap<Data, String>();
		outputs = new HashMap<Data, String>();
		
		for(Service p:graph.getPredecessors(service))
			predecessors.put(p, p.getOutputs());
		for(Service s:graph.getSuccessors(service))
			successors.put(s, s.getInputs());
		for(Data i:service.getInputs())
			inputs.put(i, null);
		for(Data o:service.getOutputs())
			outputs.put(o, null);
	}

	
	public DataMessage getDataMessage(Service destination) {
		DataMessage data = new DataMessage(service, destination);
		
		for(Data d:successors.get(destination))
			if(outputs.containsKey(d))
				data.addData(d, outputs.get(d));
		
		return data;
	}
	
	public Service getService() {
		return service;
	}
	
	public String getServiceName() {
		return service.getName();
	}

	public boolean isFireable() {
		return !inputs.containsValue(null);
	}
	
	public void addInputsValues(Map<Data, String> inputs) {
		//System.out.println(service + " " + inputs + " " + isFireable());
		//System.out.println("this.inputs = " + this.inputs + " de " + service);
		this.inputs.putAll(inputs);
		
		//System.out.println(service + " " + inputs + " " + isFireable());
		//System.out.println("this.inputs = " + this.inputs + " de " + service);
	}
	
	public List<Service> getSuccessors() {
		List<Service> successors = new ArrayList<Service>();
		
		for(Service s:this.successors.keySet())
			successors.add(s);
		return successors;	
	}

	public Map<Service, Integer> getRanks() {
		return ranks;
	}

	//TODO:remove
	public Map<Data, String> getInputs() {
		return inputs;
	}


	public void setOutputs(Map<Data, String> outputs) {
		this.outputs = outputs;
	}

	
	
	
}


