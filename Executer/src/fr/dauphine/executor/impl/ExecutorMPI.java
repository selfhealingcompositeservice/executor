package fr.dauphine.executor.impl;

import mpi.MPI;
import fr.dauphine.enginethread.EngineThread;
import fr.dauphine.executionengine.ExecutionEngine;
import fr.dauphine.executor.Constants;
import fr.dauphine.executor.Environment;
import fr.dauphine.executor.Executor;

public class ExecutorMPI implements Executor {

	private int rank;
	String filename, qosFilename, dataFilename, queryFilename;


	public ExecutorMPI(String filename, String qosFilename,
			String dataFilename, String queryFilename) {
		this.filename = filename;
		this.qosFilename = qosFilename;
		this.dataFilename = dataFilename;
		this.queryFilename = queryFilename;
	}

	@Override
	public void execute() {
		rank = MPI.COMM_WORLD.Rank();
		
		if(rank == Constants.EXECUTION_ENGINE) {
			System.out.println("Execution Engine");
			Environment environment = new Environment(filename, qosFilename, dataFilename, queryFilename);
			environment.load();
			ExecutionEngine ee = new ExecutionEngine(environment);
			ee.run();
		} else {
			System.out.println("Engine Thread");
			EngineThread et = new EngineThread();
			et.run();
		}
	}

	public static void main(String...args) {
		MPI.Init(args);
		
		(new ExecutorMPI("cwsdummy.txt", "servicesQoSdummy.txt",
				"cwsdummy_data.txt", "querydummy.txt")).execute();
		
		MPI.Finalize();
	}
}
