package fr.dauphine.message;

import java.util.Map;

import mpi.MPI;
import mpi.Request;
import fr.dauphine.executor.Constants;
import fr.dauphine.executor.EngineThreadInfo;
import fr.dauphine.logger.ServiceData;
import fr.dauphine.service.Service;

public class Message {
	private Object[] message;
	private Service destination;
	private static Map<Service, Integer> ranks;
	
	public Message() {
		this.message = new Object[1];
	}
	
	public Message(Object message, Service destination) {
		this();
		this.message[0] = message;
		this.destination = destination;
	}
	
	public Message(Object message, int destination) {
		this();
		this.message[0] = message;
		
		for(Service s:ranks.keySet())
			if(ranks.get(s).equals(destination)) {
				this.destination = s;
				break;
			}
	}
	
	public void send() {
		MPI.COMM_WORLD.Isend(message, 0, 1, MPI.OBJECT, ranks.get(destination), 0);
	}
	
	public void receive() {
		Request rreq = MPI.COMM_WORLD.Irecv(message, 0, 1, MPI.OBJECT, MPI.ANY_SOURCE, 0);
		rreq.Wait();
	}
	
	public boolean isString() {
		return message[0] instanceof String;
	}
	
	public boolean isFinalize() {
		return Constants.Messages.equals(Constants.Messages.FINALIZE , message[0]);
	}
	
	public boolean isInitialize() {
		return message[0] instanceof EngineThreadInfo;
	}
	
	public boolean isDataMessage() {
		return message[0] instanceof DataMessage;
	}
	
	public static void setRanks(Map<Service, Integer> ranks) {
		Message.ranks = ranks;
	}

	public Object getMessage() {
		return message[0];
	}
	
	public EngineThreadInfo getEngineThreadInfo() {
		if(isInitialize())
			return (EngineThreadInfo) message[0];
		return null;
	}
	
	public DataMessage getDataMessage() {
		if(isDataMessage())
			return (DataMessage) message[0];
		return null;
	}

	public boolean isServiceDataMessage() {
		return message[0] instanceof ServiceData;
	}

	public ServiceData getServiceDataMessage() {
		if(isServiceDataMessage())
			return (ServiceData) message[0];
		return null;
	}

}
