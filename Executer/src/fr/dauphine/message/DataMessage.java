package fr.dauphine.message;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import fr.dauphine.service.Data;
import fr.dauphine.service.Service;

public class DataMessage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Service senderService;
	private Service receiverService;
	private Map<Data, String> data;
	
	
	public DataMessage(Service senderService, Service receiverService) {
		super();
		this.senderService = senderService;
		this.receiverService = receiverService;
	}
	
	public DataMessage(Service senderService, Service receiverService, 
			Map<Data, String> data) {
		this(senderService, receiverService);
		this.data = data;
	}

	public Service getSenderService() {
		return senderService;
	}
	
	public Service getReceiverService() {
		return receiverService;
	}

	public Map<Data, String> getData() {
		return data;
	}
	
	public void addData(Data d, String value) {
		if(data == null)
			data = new HashMap<Data, String>();
		
		data.put(d, value);
	}
	
	
}
