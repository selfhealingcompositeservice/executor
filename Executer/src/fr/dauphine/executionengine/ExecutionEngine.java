package fr.dauphine.executionengine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import fr.dauphine.executor.Constants;
import fr.dauphine.executor.EngineThreadInfo;
import fr.dauphine.executor.Environment;
import fr.dauphine.graphutils.GraphUtils;
import fr.dauphine.logger.Logger;
import fr.dauphine.logger.ServiceData;
import fr.dauphine.message.DataMessage;
import fr.dauphine.message.Message;
import fr.dauphine.service.Data;
import fr.dauphine.service.Service;
import mpi.MPI;

public class ExecutionEngine {

	private Environment environment;
	private EngineThreadInfo etI, etF;
	private Map<String, ServiceData> executionData;
	
	public ExecutionEngine(Environment environment) {
		this.environment = environment;
		Message.setRanks(environment.getRanks());
	}
	
	public void run() {
		System.out.println("Running EE rank:" + MPI.COMM_WORLD.Rank());
		
		//task iii. start engine threads: send initialization values
		initializeThreads();
		
		//task iv: send values I_Q to Execution Threads representing (WSee_i*)*
		sendInputsValues();
		
		finalPhase();
		
		System.out.println(executionData);
		
		recordExecution();
	}
	
	private void initializeThreads() {
		for(Service s:environment.getGraph().getVertices()) {
			EngineThreadInfo et = new EngineThreadInfo(s, environment.getGraph(), environment.getRanks());
			if(!s.isControl())
				(new Message(et, s)).send();
			else
				if(s.isInitial())
					etI = et;
				else
					etF = et;
		}
	}
	
	private void sendInputsValues() {
		Service i = GraphUtils.getVertexByName(environment.getGraph(), fr.dauphine.service.Constants.INITIAL_NODE);
		for(Service s:environment.getGraph().getSuccessors(i)) {
			System.out.println("Sending inputs to " + s);
			DataMessage dm = new DataMessage(i, s);
			for(Data d:s.getInputs())
				dm.addData(d, d.getName());
			(new Message(dm, s)).send();
		}
				
	}
	
	private void finalPhase() {
		
		waitOutputs();
	
		for(Service s:environment.getGraph().getVertices())
			if(!s.isControl())
				(new Message(Constants.Messages.FINALIZE, s)).send();
		
		//collect execution data
		collectExecutionData();
	}

	private void waitOutputs() {
		System.out.println("waitOutputs " + etF.getInputs() + " name: " + etF.getServiceName());
		while(!etF.isFireable()) {
			Message m = new Message();
			m.receive();
			if(m.isDataMessage())
				etF.addInputsValues(m.getDataMessage().getData());
		}
	}
	
	private void collectExecutionData() {
		executionData = new HashMap<String, ServiceData>();
		
		//initialize
		for(Service s:environment.getGraph().getVertices())
			if(!s.isControl())
				executionData.put(s.getName(), null);
		
		//wait for the data
		while(executionData.containsValue(null)) {
			Message m = new Message();
			m.receive();
			if(m.isServiceDataMessage()) {
				ServiceData d = m.getServiceDataMessage();
				executionData.put(d.getServiceName(), d);
			}
		}
		
	}
	
	private void recordExecution() {
		GraphUtils.removeControlNodes(environment.getGraph());
		System.out.println(new ArrayList<ServiceData>(executionData.values()));
		Logger logger = new Logger(
				new ArrayList<ServiceData>(executionData.values()), 
				environment.getGraph(), 
				0.0);
		logger.writeSingleServices();
	}
	
}
