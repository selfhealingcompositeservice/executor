package fr.dauphine.serviceexecution;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import fr.dauphine.service.Data;
import fr.dauphine.service.Service;


/**
 * Handles the execution of a Service
 *
 * @author Rafael Angarita
 * @version 1.0
 */
public class RunService {
	
	Service service;
	double executionTime;
	boolean success;
	Timestamp timeStamp;
	
	public RunService(Service service) {
		this.service = service;
	}
	
	public void run() {
		try {
			success = !simulateServiceFailure();
			executionTime = simulateExecutionTime(success);
			java.util.Date currentDate = new java.util.Date();
			timeStamp = new Timestamp(currentDate.getTime());
			Thread.sleep((long) executionTime);
			
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		
	}
	
	public double getExecutionTime() {
		return executionTime;
	}
	
	public boolean getSuccess() {
		return success;
	}
	
	public Timestamp getTimeStamp() {
		return timeStamp;
	}
	
	//simulate execution time
	private double simulateExecutionTime(boolean success) {
		if(!success)
			return randInt(0, service.getEstimatedExecutionTime() + service.getEstimatedExecutionTime()/2);
		else
			return service.getEstimatedExecutionTime();
	}

	//simulate probabilty of a failure
	//returns true if the service fails
	private boolean simulateServiceFailure() {
		if(Math.random() < service.getFailureProbability())
		   return true;
		return false;
	}
	
	private static int randInt(double min, double max) {
	    Random rand = new Random();
	    int randomNum = (int) (rand.nextInt((int) ((max - min) + 1)) + min);

	    return randomNum;
	}
	
	//dummy output generation
	public Map<Data, String> dummyGenerateOutputs() {
		Map<Data, String> outputs = new HashMap<Data, String>();
		for(Data o:service.getOutputs())
			outputs.put(o, o.getName());
		return outputs;
	}

}